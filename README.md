# Microservices Example

This project provides an example of how to develop for a microservices architecture using a Visual Studio Code Dev Container.

## Features

- Express backend running in a Docker container.
- MongoDB database running in a Docker container.
- Vue.js frontend development server
- [Swagger Viewer VS Code extension](https://marketplace.visualstudio.com/items?itemName=Arjun.swagger-viewer) (
arjun.swagger-viewer) to allow viewing API
- [REST Client VS Code extension](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) (humao.rest-client) to allow API calls to be made

## Requires

* Docker
* VS Code
* VS Code extension "Remote - Containers"

## Using

### Running Microservices Example

1. Open a terminal in VS Code.
2. Enter `yarn install` to install dependencies
3. Start backend server and load example data
    - `bin/backend-down.sh`
    - `bin/backend-up.sh`
    - `bin/backend-data.sh`
4. Enter `yarn serve` to start frontend development server
5. Open a browser to [http://localhost:8080/](http://localhost:8080/)

### Viewing API Specification

1. Open [`items-api.yaml`](items-api.yaml)
2. Go to `Command Palette` (Crtl+Shift+P)
3. Choose `Preview Swagger`

### Make API Calls to Backend Server

1. Open [`calls.http`](calls.http)
2. Click on `Send Request` above any call
3. View results in `Response` window

## Notes

1. This is an example that is meant to show you how to run a microservices architecture running multiple servers in Docker containers.
2. The is not the way you would run this in production.
3. MongoDB Docker image is pinned to version 8.

---
Copyright &copy; 2022 The HFOSSedu Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
