#!/usr/bin/env bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}/.."

bin/backend-down.sh
bin/backend-up.sh
sleep 3
bin/backend-data.sh
